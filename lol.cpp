#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(){
	ifstream arquivo("lena-1.pgm");
	ofstream novo("lena-3.pgm");
	string numeroMagico, comentario;
	int largura, altura, i, j, x, y, teste, div=9, value, filter[9]= {1,1,1,1,1,1,1,1,1}, max;
	char *m, *mc, k;
	
	getline(arquivo, numeroMagico);
	novo << numeroMagico << endl;
	getline(arquivo, comentario);
	novo << comentario << endl;
	arquivo >> largura >> altura >> max;
	arquivo.get(k);
	novo << largura << ' ' << altura << endl << max << endl;

	m = new char[largura*altura];
	mc = new char[largura*altura];

	cout << "Digite filtro(0 para smooth - 1 para sharpen): ";
	cin >> teste;

	if(teste){
		cout << '{';
		for(i=0; i<9;i++){
			if(i)
				cout << ',';
			filter[i] = (-i)%2;
			if(i == 4)
				filter[i] = 5;
			cout << filter[i];
		}cout << "}\n";
		div = 1;
	}
	
	for(i = 0; i < largura; i++){
		for(j = 0; j < altura; j++){
			arquivo.get(m[i*largura+j]);
			m[i*largura+j] = (char)(max-m[i*largura+j]);
			mc[i*largura+j] = m[i*largura+j];
			
		}
	}
	arquivo.close();
	
	for(i=1; i<largura-1; i++){
		for(j=1; j<altura-1; j++){
			value = 0;
			for(x=-1; x<=1; x++){
				for(y=-1; y<=1;y++){
					value += filter[3*(x+1)+(y+1)]*(unsigned char)m[(i+x)*largura+(y+j)];
				}
			}
				value /= div;
				
				value = value < 0? 0 : value;
				value = value > 255? 255 : value;
				
				mc[i*largura+j] = value;
		}
	}
	
	for(i = 0; i < largura; i++){
		for(j = 0; j < altura; j++){
				novo.put(max-mc[i*largura+j]);
		}
	}
	
	novo.close();
	
	return 0;
}
